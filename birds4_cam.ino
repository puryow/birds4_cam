/*
 * FM Software for MSN BRD II CAM MSN 
 * FM v1.0 
 * Initial Release. Simplest Working Flight Model Software. No software redundancy. 
 * 23.11.2018
 * 
 * 1) Handshake             :0x54 (constant)
 * 2) Select Resolution     :320x240 (default)
 * 3) Select Quality        :low (default)
 */


//INCLUDES
#include <Wire.h>
#include <ArduCAM.h>
#include <SPI.h>
#include <FM_lib.h>
#include <memorysaver.h>

//CAM
const int CAM_CS  =         22;
const int CAM_ON  =         38;       //Turn OV Camera ON, Active HIGH
const int LED3    =         4 ;       //Debug LED
bool is_header    =      false;
int mode          =          0;
int flag          =          0;
uint8_t start_capture   =    0;
ArduCAM myCAM(OV5642, CAM_CS);
uint8_t read_fifo_burst(ArduCAM myCAM);
int resolution    =          0; 
int quality       =          0;
int select_type   =          0;

//FM
//const int FM_CS   = 24;             //CS defined on FM_lib.h
long OBC_address = 0x00080000;        //FM address on OBC

//Setup that runs once
void setup() {

  //CAM Setup
  //uint8_t thumb;	                    //Check register value to check thumbnail
  uint8_t vid, pid;
  uint8_t temp;
  
  //Start the system, REMOVE IN FM
  pinMode(4, OUTPUT);
  digitalWrite(4, HIGH);              //Turn on LED of Cam Mission Board, FM Modify
  delay(1000);                        //Delay 

  //Communication channels initialize
  Wire.begin();                       //I2C Communication with OV5642 Sensor
  Serial.begin(115200);               //Actually runs at 57600, FM Modify
  Serial1.begin(19200);               //Depends on communication with OBC. Runs at (19200/2 speed)
  
  //UART1 Read
  int handshake = 0;

  Serial.println ("Send Trigger");    //FM Modify

  //Handshake with OBC
  while (1) { 
    //Serial.println("CHECK");        //FM Modify
    
    if (Serial1.available() > 0){
      //digitalWrite(4, HIGH);  
      handshake = Serial1.read();
      Serial.println (handshake, HEX);              //FM Modify
      if (handshake == 0x54){
        Serial.println("Correct Value Received");   //FM Modify
        delay(2000);
        Serial1.write(0x61);                        //Sends unique ID: 0x61
        break;  
      }
      else {
        continue;
      }
    }
  }

  //Resolution select by OBC
  Serial.println ("Select Type");
  while (1) {
    //Serial.println("Select Resolution");          //FM Modify
    if (Serial1.available() > 0) {
      select_type = Serial1.read();
      Serial.println(select_type, HEX);              //FM Modify
      if (select_type == 0xA0) {
        resolution = 0x10;
        quality    = 0xD1;
        Serial.println ("Resolution set to 320x240, Quality low");
        delay(2000);                                //Let OBC move to listening mode
        Serial1.write(0xA0);
        break;
        }
        
      else if (select_type == 0xA1) {
        resolution = 0x10;
        quality    = 0xD2;
        Serial.println ("Resolution set to 320x240, Quality normal");
        delay(2000);                                //Let OBC move to listening mode
        Serial1.write(0xA1);
        break;
        }

      else if (select_type == 0xA2) {
        resolution = 0x10;
        quality    = 0xD3;
        Serial.println ("Resolution set to 320x240, Quality high");
        delay(2000);                                //Let OBC move to listening mode
        Serial1.write(0xA2);
        break;
        }

      else if (select_type == 0xB0) {
        resolution = 0x20;
        quality    = 0xD1;
        Serial.println ("Resolution set to 640x480, Quality low");
        delay(2000);                                //Let OBC move to listening mode
        Serial1.write(0xB0);
        break;
        }

      else if (select_type == 0xB1) {
        resolution = 0x20;
        quality    = 0xD2;
        Serial.println ("Resolution set to 640x480, Quality normal");
        delay(2000);                                //Let OBC move to listening mode
        Serial1.write(0xB1);
        break;
        }        

      else if (select_type == 0xB2) {
        resolution = 0x20;
        quality    = 0xD3;
        Serial.println ("Resolution set to 640x480, Quality high");
        delay(2000);                                //Let OBC move to listening mode
        Serial1.write(0xB2);
        break;
        }
                                
      else {
        continue;
        }     
    }
  }

/*
  //Select quality by OBC
  Serial.println("Select Quality");             //FM Modify
  while (1) {
    if (Serial1.available() > 0) {
      quality = Serial1.read();
      Serial.println(quality, HEX);             //FM Modify

      if (quality == 0xD1) {
        Serial.println ("Quality set at low");
        delay(2000);
        Serial1.write(0xD1);
        break;
        }
      else if (quality == 0xD2) {
        Serial.println ("Quality set at default");
        delay(2000);
        Serial1.write(0xD2);
        break;
        }
      else if (quality == 0xD3) {
        Serial.println ("Quality set at high");
        delay(2000);
        Serial1.write(0xD3);
        break;
        }
      else {
        continue;
        }     
      
    }
  }
*/    
  Serial.println(F("ACK CMD ArduCAM Start!"));
  
  pinMode(CAM_ON, OUTPUT);
  digitalWrite(CAM_ON,HIGH);          //Turn on OV5642 Camera
  delay(1000);
  
  // set the CS as an output:
  pinMode(CAM_CS, OUTPUT);
  
  // initialize SPI:
  SPI.begin();
  
  while(1){
    //Check if the ArduCAM SPI bus is OK
    myCAM.write_reg(ARDUCHIP_TEST1, 0x61);
    temp = myCAM.read_reg(ARDUCHIP_TEST1);
    Serial.print("Checking HEX write/read value on SPI (0x61)=0x");
    Serial.println(temp,HEX);
    if (temp != 0x61){
      Serial.println(F("ACK CMD SPI interface Error!"));
      delay(1000);continue;
    }else{
      Serial.println(F("ACK CMD SPI interface OK."));break;
    }
  }
  while(1){
    //Check if the camera module type is OV5642
    myCAM.wrSensorReg16_8(0xff, 0x01);
    myCAM.rdSensorReg16_8(OV5642_CHIPID_HIGH, &vid);
    myCAM.rdSensorReg16_8(OV5642_CHIPID_LOW, &pid);
    if((vid != 0x56) || (pid != 0x42)){
      Serial.println(F("ACK CMD Can't find OV5642 module!"));
      delay(1000);continue;
    }
    else{
      Serial.println(F("ACK CMD OV5642 detected through I2C."));break;
    } 
  }
  
  //Change to JPEG capture mode and initialize the OV5642 module
  myCAM.set_format(JPEG); /*Clear*/
  myCAM.InitCAM(); /*Clear*/
  myCAM.write_reg(ARDUCHIP_TIM, VSYNC_LEVEL_MASK);   //VSYNC is active HIGH
  
  //Resolution by default
  myCAM.OV5642_set_JPEG_size(OV5642_320x240);
  //Quality by default
  myCAM.OV5642_set_Compress_quality(low_quality);

  //Set resolution
  if (resolution == 0x10) {
    myCAM.OV5642_set_JPEG_size(OV5642_320x240);
  }
  else if (resolution == 0x20) {
    myCAM.OV5642_set_JPEG_size(OV5642_640x480);
  }

  //Set quality
  if (quality == 0xD1) {
    myCAM.OV5642_set_Compress_quality(low_quality);
  }
  else if (quality == 0xD2) {
    myCAM.OV5642_set_Compress_quality(default_quality);
  }
  else if (quality == 0xD3) {
    myCAM.OV5642_set_Compress_quality(high_quality);
  }
  delay(1000);
  myCAM.clear_fifo_flag();
  myCAM.write_reg(ARDUCHIP_FRAMES,0x00);
  Serial.println("Initialization Complete");
  pinMode(FM_CS, OUTPUT);
  Serial.println();
  Serial.println("BIRDS-3 Cam Board OBC CAM FM2 Test");
  SPI.begin();
}

void loop() {
  // put your main code here, to run repeatedly:

if (flag == 0) {
  uint8_t temp = 0xff, temp_last = 0;
  bool is_header = false;

      Serial.println("Start:");
      //start_capture = 49;

      myCAM.flush_fifo();
      myCAM.clear_fifo_flag();
      //Start capture
      myCAM.start_capture();

      delay(100);//Necessary for ARDUCHIP_TRIG and CAP_DONE_MASK to be set
     
     if (myCAM.get_bit(ARDUCHIP_TRIG, CAP_DONE_MASK)) {
      Serial.println(F("ACK CMD CAM Capture Done."));
      read_fifo_burst(myCAM);
      //Clear the capture done flag
      myCAM.clear_fifo_flag();
     }
   }
   flag = 1;  
}


//CAM Function
uint8_t read_fifo_burst(ArduCAM myCAM)  {
  uint8_t temp = 0, temp_last = 0;
  uint32_t length = 0;
  length = myCAM.read_fifo_length();
  Serial.println(length, DEC);
  Serial.println("First Test Break");

  if (length >= MAX_FIFO_SIZE) //512 kb
  {
    Serial.println(F("Over size."));
    return 0;
  }
  if (length == 0 ) //0 kb
  {
    Serial.println(F("Size is 0."));
    return 0;
  }
  myCAM.CS_LOW();
  myCAM.set_fifo_burst();//Set fifo burst mode
  byte image_buffer [256];
  int i = 0, j = 0;
  uint32_t counter = 0;
  length --;
  while ( length-- )
  {
      temp_last = temp;
      temp =  SPI.transfer(0x00);
      
      if (is_header == true)
      {
       Serial.print(temp, HEX);
       image_buffer [i++] = temp;
       counter++;
      }
      else if ((temp == 0xD8) && (temp_last == 0xFF))
      {
        is_header = true;

        Serial.println(F("Write to FM from CAM Data:"));
        Serial.print(temp_last, HEX);
        image_buffer [i++] = temp_last;
        counter++;
        
        Serial.print(temp,HEX);
        image_buffer [i++] = temp;
        counter++;
     
      }
      if ( (temp == 0xD9) && (temp_last == 0xFF) ) { //If find the end ,break while,
        myCAM.CS_HIGH();
        Serial.println();
        delay(100);
        write_enable();
        write_sector(OBC_address, image_buffer);
        delay (100);
        read_sector(OBC_address);
        delay(100);
        Serial.println();
        Serial.println(counter, DEC);
        Serial.println ("TEST WRITE COMPLETE");
        break;
      }
      delayMicroseconds(15); 
      if (i == 256)  {
        myCAM.CS_HIGH();
        Serial.println();
        
        //If reading FM for the first time
        if (j == 0) {
          j = 1;
          get_id();
          write_enable();
          Serial.println("Erase Sector of FM first time");
          erase_sector(OBC_address);
          delay(100);
          }
        write_enable();
        write_sector(OBC_address, image_buffer);
        delay (100);
        read_sector(OBC_address);
        delay (100);
        OBC_address = OBC_address + 256;
        Serial.println();
        Serial.println();
        Serial.println(OBC_address, HEX);
        i = 0;
        myCAM.CS_LOW();
        myCAM.set_fifo_burst();//Set fifo burst mod
      }
  }

  is_header = false;

  //Send 0x62 ACK to OBC
  Serial1.write(0x62); 
  delay(2000);
/*
  //Send last OBC address to 
  byte send_obc_address [4];
  send_obc_address [0] = OBC_address >> 24 & 0xFF;
  send_obc_address [1] = OBC_address >> 16 & 0xFF;
  send_obc_address [2] = OBC_address >> 8  & 0xFF;
  send_obc_address [3] = OBC_address >> 0  & 0xFF;
  for (i = 0 ; i < 4; i++){
    Serial1.write(send_obc_address[i]);
  }
*/ 
  return 1;
}
